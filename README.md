# 


## Usage:

Create a file `.env` with the API KEY. Do NOT commit this `.env` file. Add it to the `.gitignore` file.

File: `.env`
```
API_KEY="REPLACE_WITH_API_KEY"
```
