const axios = require('axios').default;
const fs = require('fs');
require('dotenv').config();

// load from the environment variable
const API_KEY = process.env.API_KEY;

axios.get('https://randomuser.me/api')
  .then((response) => {
    var jsonString = JSON.stringify(response.data, null, 4);
    fs.writeFile("user.json", jsonString, function(err) {
    if (err) {
        console.log(err);
        return console.log(err);
    }
  });
});
